/**
 * Checkout for the current SCM
 */

checkout scm
echo "application is: ${CFG.application}"
def config = gitutils.getAppConfig()
def pom = readMavenPom file: 'pom.xml'
config.group_id = pom.groupId
echo "config is: ${config}"
//def appData = [:]
OUT.'appData' = config

