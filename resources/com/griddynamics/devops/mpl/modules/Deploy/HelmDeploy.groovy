echo "config is:${CFG.appData}"
if(fileExists('helm')){
     sh "gcloud auth activate-service-account --key-file=/home/jenkins/creds.json"
     sh "gcloud container clusters get-credentials cluster-1 --zone us-central1-c --project carbon-storm-273503"
     sh "helm install hello helm/hello-spring"
}