#!groovy

def applicationPipeline(Map configMap){
    application = configMap.get("application","")
    switch(application) {
        case 'pcfMaven':
            MPLPipeline(configMap)
        break
        case 'helmMaven':
            HelmPipeline(configMap)
        break
        case 'ocpMaven':
            OpenShiftPipeline(configMap)
        break
    }
}