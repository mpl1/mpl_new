/**
 * Common deploy module
 */
//echo "config is: ${CFG}"
echo "application is: ${CFG.application}"

 if(CFG.application == "helmMaven"){
     helmUtils.deploy()
}
else if(CFG.application == "ocpMaven"){
    ocUtils.deploy()    
}
else if (CFG.application == "pcfMaven"){
    pcfutils.deploy(CFG.appData)
}
