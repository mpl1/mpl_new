//
// Copyright (c) 2018 Grid Dynamics International, Inc. All Rights Reserved
// https://www.griddynamics.com
//
// Classification level: Public
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// $Id: $
// @Project:     MPL
// @Description: Shared Jenkins Modular Pipeline Library
//

/**
 * Basic MPL pipeline
 * Shows pipeline with basic stages and modules of the MPL library
 *
 * @author Sergei Parshev <sparshev@griddynamics.com>
 */
def call(body) {
  def MPL = MPLPipelineConfig(body, [
    agent_label: 'LS-PCF',
    modules: [
      Checkout: [:],
      Build: [:],
      PCFDeploy: [:],
      Test: [:],
      TearDown: [:]
    ]
  ])

  pipeline {
    agent {
      kubernetes{
        cloud "gke"
            label "jenkins-slaves"
            yaml """
spec:
    containers:
    - name: jnlp
      image: sivakmr469/jenkins-maven-pcf:7.0.2
      imagePullPolicy: Always
      resources:
        requests:
            cpu: 0.5
            memory: 1Gi
        limits:
            cpu: 0.5
            memory: 1.5Gi
      ttyEnabled: true
      workingDir: /var/lib/jenkins
      alwaysPullImage: true

"""
      }
    }
    options {
      skipDefaultCheckout(true)
    }
    stages {
      stage( 'Checkout' ) {
        when { expression { MPLModuleEnabled() } }
        steps {
          MPLPipelineConfigMerge(MPLModule().check)
        }
      }
      stage( 'Build' ) {
        when { expression { MPLModuleEnabled() } }
        steps {
          MPLPipelineConfigMerge(MPLModule().maven)
        }
      }
      stage( 'PCFDeploy' ) {
        when { expression { MPLModuleEnabled() } }
        steps {
          MPLModule()
        }
      }
      stage( 'Test' ) {
        when { expression { MPLModuleEnabled() } }
        steps {
          MPLModule()
        }
      }
      stage( 'TearDown' ){
        when { expression { MPLModuleEnabled() } }
        steps {
          MPLModule()
        }
      }
    }
    post {
      always {
        MPLPostStepsRun('always')
      }
      success {
        MPLPostStepsRun('success')
      }
      failure {
        MPLPostStepsRun('failure')
      }
    }
  }
}
